<?php
/**
*
* Template Name: compliance
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<section class="banner_slick">
    <?php $imagem_de_fundo_compliance = get_field( 'imagem_de_fundo_compliance' ); ?>
    <?php if ( $imagem_de_fundo_compliance ) : ?>
        <img src="<?php echo esc_url( $imagem_de_fundo_compliance['url'] ); ?>" alt="<?php echo esc_attr( $imagem_de_fundo_compliance['alt'] ); ?>" />
    <?php endif; ?>
</section><!-- /.main -->

<section class="main_text_compliance">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h1><?php the_field( 'titulo_compliance', $page_ID ); ?></h1>
                <p><?php the_field( 'descricao_compliance', $page_ID ); ?></p>
            </div>
        </div>
    </div>
</section>

<section class="etica">
    <div class="container-fluid">
        <div class="row d-flex justify-content-between">
            <div class="col-md-5 card-content">
                <?php the_field( 'descricao_codigo_de_etica_e_conduta', $page_ID ); ?><br>
                <strong><?php the_field( 'saiba_mais_codigo_de_etica_e_conduta', $page_ID ); ?></strong>
            </div>
            <div class="col-md-4 card-img">
                <?php $imagem_codigo_de_etica_e_conduta = get_field( 'imagem_codigo_de_etica_e_conduta' ); ?>
                <?php if ( $imagem_codigo_de_etica_e_conduta ) : ?>
                    <img src="<?php echo esc_url( $imagem_codigo_de_etica_e_conduta['url'] ); ?>" alt="<?php echo esc_attr( $imagem_codigo_de_etica_e_conduta['alt'] ); ?>" loading='lazy' />
                <?php endif; ?>
                <?php if ( get_field( 'arquivo_do_codigo_de_etica_e_conduta' ) ) : ?><br>
                    <a href="<?php the_field( 'arquivo_do_codigo_de_etica_e_conduta', $page_ID ); ?><br>"><?php the_field( 'texto_do_botao_codigo_de_etica_e_conduta', $page_ID ); ?><br></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<section class="comunicacao">
    <div class="container-fluid">
        <div class="bg-blue row d-flex justify-content-center">
            <div class="col-md-9 pb-5">
                <h2>Comunicação</h2>
            </div>
            <ul class="list-box col-md-9">
                <?php if ( have_rows( 'box_post' ) ) : ?>
                    <?php while ( have_rows( 'box_post' ) ) : the_row(); ?>
                        <li class="card col-md-4">
                            <div class="card-header">
                                <?php if ( get_sub_field( 'imagem_ou_video' ) == 1 ) : ?>
                                    <?php the_sub_field( 'box_video' ); ?>
                                <?php else : ?>
                                    <?php $box_imagem = get_sub_field( 'box_imagem' ); ?>
                                    <?php if ( $box_imagem ) : ?>
                                        <img src="<?php echo esc_url( $box_imagem['url'] ); ?>" alt="<?php echo esc_attr( $box_imagem['alt'] ); ?>" />
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                            <div class="card-content">
                                <h2><?php the_sub_field( 'box_title' ); ?></h2>
                                <?php the_sub_field( 'box_conteudo' ); ?>
                            </div>
                        </li>
                    <?php endwhile; ?>
                <?php else : ?>
                    <?php // no rows found ?>
                <?php endif; ?>
            </ul>
        </div>
        <div class="materiais row d-flex justify-content-center">
            <div class="col-md-4">
                <h2>Materiais CSS</h2>
                    <ul class="list-materiais">
                        <?php if ( have_rows( 'materiais_css' ) ) : ?>
                            <?php while ( have_rows( 'materiais_css' ) ) : the_row(); ?>
                                <li class="col-md-9 mb-3">
                                    <div class="card-icon mr-3">
                                        <?php the_sub_field( 'icon_documento' ); ?>
                                    </div>
                                    <div class="card-content mr-3">
                                        <?php $documentos_css = get_sub_field( 'documentos_css' ); ?>
                                        <?php if ( $documentos_css ) : ?>
                                            <a href="<?php echo esc_url( $documentos_css['url'] ); ?>"><?php the_sub_field( 'title_documento' ); ?></p></a>
                                        <?php endif; ?>
                                    </div>
                                </li>
                            <?php endwhile; ?>
                        <?php else : ?>
                            <?php // no rows found ?>
                        <?php endif; ?>
                    </ul>
                </div>

                <div class="col-md-4 pl-0 pr-4 card-contact">
                    <div class="col-md-3 p-0 img-contact">
                        <?php $imagem_contato = get_field( 'imagem_contato' ); ?>
                        <?php if ( $imagem_contato ) : ?>
                            <img src="<?php echo esc_url( $imagem_contato['url'] ); ?>" alt="<?php echo esc_attr( $imagem_contato['alt'] ); ?>" />
                        <?php endif; ?>
                    </div>
                    <div class="col-md-8 box-contact pl-0">
                        <h2><?php the_field( 'title_mailchimp' ); ?></h2>
                        <p><?php the_field( 'descricao_mailchimp' ); ?></p>
                        <a href="<?php the_field( 'link_form' ); ?>">Click aqui</a>
                    </div>
                </div>
        </div>
    </div>
</section>

<section class="denuncia">
    <div class="container-fluid">
        <div class="row d-flex justify-content-between">
            <div class="col-md-5 card-content">
                    <h2><?php the_field( 'titulo_canal_de_denuncia', 'option' ); ?></h2>
                    <p><?php the_field( 'descricao_canal_de_denuncia', 'option' ); ?></p>
            </div>
            <div class="col-md-6 card-contact">
                <?php $imagem_canal_de_denuncia = get_field( 'imagem_canal_de_denuncia', 'option' ); ?>
                <?php if ( $imagem_canal_de_denuncia ) : ?>
                    <img class="hiden-mobile" src="<?php echo esc_url( $imagem_canal_de_denuncia['url'] ); ?>" alt="<?php echo esc_attr( $imagem_canal_de_denuncia['alt'] ); ?>" loading='lazy' />
                <?php endif; ?>
                <br>
                <div class="contact-dados">
                    <div class="title">
                        <strong><?php the_field( 'ajude-nos_canal_de_denuncia', 'option' ); ?></strong>
                    </div>
                    <div class="content">
                        <p><a href="<?php the_field( 'site_canais_de_denuncia', 'option' ); ?>">compliance-office.com/css</a></p>
                        <p>
                        <?php $icon_telefone = get_field( 'icon_telefone', 'option' ); ?>
                        <?php if ( $icon_telefone ) : ?>
                            <img src="<?php echo esc_url( $icon_telefone['url'] ); ?>" alt="<?php echo esc_attr( $icon_telefone['alt'] ); ?>" loading='lazy'/>
                        <?php endif; ?>
                            
                        <?php the_field( 'telefone_canais_de_denuncia', 'option' ); ?></p>
                        <p>
                        <?php $icon_e_mail = get_field( 'icon_e-mail', 'option' ); ?>
                        <?php if ( $icon_e_mail ) : ?>
                            <img src="<?php echo esc_url( $icon_e_mail['url'] ); ?>" alt="<?php echo esc_attr( $icon_e_mail['alt'] ); ?>" loading='lazy' />
                        <?php endif; ?>    
                        
                        <?php the_field( 'e-mail_canais_de_denuncia', 'option' ); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>