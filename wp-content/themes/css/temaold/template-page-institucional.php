<?php
/**
*
* Template Name: institucional
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<div id="carouselbanner" class="carousel slide carousel-fade" data-ride="carousel" data-interval="3000">
  <div class="carousel-inner">

  <?php if ( have_rows( 'cadastro_de_imagens' ) ) : ?>
    <?php
        $cont = 1;
    ?>
        <?php while ( have_rows( 'cadastro_de_imagens' ) ) : the_row(); ?>
            <div class="carousel-item <?php echo $cont == 1 ? 'active' : ''; ?>">
                <?php $imagem_de_fundo = get_sub_field( 'imagem_de_fundo' ); ?>
                <?php if ( $imagem_de_fundo ) : ?>
                    <img src="<?php echo esc_url( $imagem_de_fundo['url'] ); ?>" alt="<?php echo esc_attr( $imagem_de_fundo['alt'] ); ?>" loading='lazy' />
                <?php endif; ?> 
            </div>  
            <?php $cont++; ?>
        <?php endwhile; ?>
    <?php else : ?>
        <?php // no rows found ?>
    <?php endif; ?>
  </div>
</div>


<section class="main_text_about">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <h1><?php the_field( 'titulo_quem_somos', $page_ID ); ?></h1>
            </div>
        </div>
    </div>
</section>

<section class="about_container">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-10">
				<h2><?php the_field( 'titulo_uma_empresa_com_base_solida', $page_ID ); ?></h2>
				<?php the_field( 'descricao_uma_empresa_com_base_solida', $page_ID ); ?>
			</div>
		</div>
	</div>
</section>

<section class="nosso-jeito">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<h2><?php the_field( 'titulo_nosso_jeito', $page_ID ); ?></h2>
				<p><?php the_field( 'descricao_nosso_jeito', $page_ID ); ?></p>
			</div>
			<div class="col-md-10">
				<ul class="list-nj">
					<?php if ( have_rows( 'cadastro_de_icones', $page_ID ) ) : ?>
						<?php while ( have_rows( 'cadastro_de_icones', $page_ID ) ) : the_row(); ?>
							<li class="card">
								<div class="card-header">
									<?php $icone = get_sub_field( 'icone' ); ?>
									<?php if ( $icone ) : ?>
										<img src="<?php echo esc_url( $icone['url'] ); ?>" alt="<?php echo esc_attr( $icone['alt'] ); ?>" loading='lazy'/>
									<?php endif; ?>
								</div>
								<div class="card-content">
									<p><?php the_sub_field( 'titulo' ); ?></p>
								</div>
							</li>
						<?php endwhile; ?>
					<?php else : ?>
						<?php // no rows found ?>
					<?php endif; ?>
				</ul>
			</div>
		</div>
	</div>
</section>

<section class="con-obras">
	<div class="about-atuacao">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<h2><?php the_field( 'titulo_atuacao', $page_ID ); ?></h2>
						<p><?php the_field( 'descricao_atuacao', $page_ID ); ?></p>
						<?php $botao_obras = get_field( 'botao_obras', $page_ID ); ?>
					</div>
					<?php if ( $botao_obras ) : ?>
						<a href="<?php echo esc_url( $botao_obras['url'] ); ?>" target="<?php echo esc_attr( $botao_obras['target'] ); ?>"><?php echo esc_html( $botao_obras['title'] ); ?></a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>

	<div class="certificacao">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<h2>
						<?php the_field( 'titulo_certificacoes', $page_ID ); ?><br><br>
						<?php the_field( 'abnt_certificacoes', $page_ID ); ?>
					</h2>
					<p><?php the_field( 'descricao_certificacoes', $page_ID ); ?></p>
				</div>
				<div class="col-md-11">
					<ul class="list-ct">
						<?php if ( have_rows( 'cadastro_de_certificacoes', $page_ID ) ) : ?>
							<?php while ( have_rows( 'cadastro_de_certificacoes', $page_ID ) ) : the_row(); ?>
								<li class="card">
									<h2>
										<?php the_sub_field( 'titulo' ); ?><br>
										<?php the_sub_field( 'nº' ); ?><br>
										<?php the_sub_field( 'subtitulo' ); ?>
									</h2>
									<p><?php the_sub_field( 'descricao' ); ?></p>
								</li>
							<?php endwhile; ?>
						<?php else : ?>
							<?php // no rows found ?>
						<?php endif; ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>