<!-- Footer -->
<footer>
    <div class="container h-100">
        <div class="row h-100 align-items-stretch justify-content-end">
            <div class="col-md-12 d-flex justify-content-between">
                <!-- Navigation -->
                <img class="footer-logo" src='<?php the_field('logo_site', 'option') ?>' class='img-fluid' alt='<?php bloginfo( 'name' ); ?>' title='<?php bloginfo( 'name' ); ?>' loading='lazy'>

                <nav class="navbar navbar-expand-md navbar-default p-0" role="navigation">
                    <div class="container p-0">
                
                        
                        <?php
                        wp_nav_menu( array(
                            'theme_location'    => 'primary',
                            'depth'             => 2,
                            'container'         => 'div',
                            'container_class'   => 'collapse navbar-collapse',
                            'container_id'      => 'bs-example-navbar-collapse-1',
                            'menu_class'        => 'nav navbar-nav ml-auto align-items-center',
                            'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                            'walker'            => new WP_Bootstrap_Navwalker(),
                        ) );
                        ?>
                    </div>
                </nav>
            </div>            
            <div class="col-md-12 footer-content">
                <div class="col-md-4 p-0    ">
                    <br><h4>Contato</h4><br>
                    <p><b>Telefone:</b> <?php the_field( 'telefone_contato', 'option' ); ?></p>
                    <p><b>E-mail:</b> <?php the_field( 'e-mail_contato', 'option' ); ?></p>
                </div>
                <div class="col-md-4 p-0">
                    <br><h4>Onde estamos</h4><br>
                    <p><?php the_field( 'endereco', 'option' ); ?></p>
                </div>
                <div class="col-md-4 p-0">
                    <br><h4>Canais de denuncia</h4><br>
                    <p><b>Site:</b> <a href="<?php the_field( 'site_canais_de_denuncia', 'option' ); ?>"><?php the_field( 'site_canais_de_denuncia', 'option' ); ?></a></p>
                    <p><b>E-mail:</b> <?php the_field( 'e-mail_canais_de_denuncia', 'option' ); ?></p>
                    <p><b>Telefone:</b> <?php the_field( 'telefone_canais_de_denuncia', 'option' ); ?></p>
                </div>
                <div class="col-md-1 p-0 social-medias">
                    
                <?php if ( have_rows( 'cadastro_de_redes_sociais', 'option' ) ) : ?>
                    <?php while ( have_rows( 'cadastro_de_redes_sociais', 'option' ) ) : the_row(); ?>
                    <a href=" <?php the_sub_field( 'link' ); ?>">
                        <?php the_sub_field( 'icone_' ); ?>
                    </a>
                    <?php endwhile; ?>
                <?php else : ?>
                    <?php // no rows found ?>
                <?php endif; ?>




                </div>
                

            </div>
        </div>
    </div>
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" integrity="sha512-F5QTlBqZlvuBEs9LQPqc1iZv2UMxcVXezbHzomzS6Df4MZMClge/8+gXrKw2fl5ysdk4rWjR0vKS7NNkfymaBQ==" crossorigin="anonymous"></script>-->
</footer>

  
<?php wp_footer(); ?>

</body>

</html>
