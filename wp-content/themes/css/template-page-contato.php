<?php
/**
*
* Template Name: contato
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>



<?php if ( have_rows( 'cadastro_de_formularios', $page_ID ) ) : ?>
	<?php while ( have_rows( 'cadastro_de_formularios', $page_ID ) ) : the_row(); ?>
		<?php the_sub_field( 'titulo' ); ?>
		<?php if ( get_sub_field( 'icone' ) ) : ?>
			<img src="<?php the_sub_field( 'icone' ); ?>" loading='lazy' />
		<?php endif ?> 
        <?php the_sub_field( 'formulario_de_contato' ); ?>

	<?php endwhile; ?>
<?php else : ?>
	<?php // no rows found ?>
<?php endif; ?>






<section class="contact-us">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="box-content">
					<h1><?php the_field( 'titulo', $page_ID ); ?></h1>
					<p><?php the_field( 'descricao', $page_ID ); ?></p>
				</div>
			</div>
			<div class="col-md-6 forms">
				<div class="nav nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
					<a class="fale-con nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
						<div class="img">
							<i class="fale" alt="Ícone com dois balões para simbolizar o diálogo. "></i>
						</div>
						<div class="text">
							<?php the_field( 'fale_conosco_titulo' ); ?>
						</div>
					</a>
					<a class="trab-con nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">
						<div class="img">
							<i class="trabalhe" alt="Ícone com um aperto de mãos para simbolizar um contato profissional. "></i> 
						</div>
						<div class="text">
							<?php the_field( 'trabalhe_conosco_titulo' ); ?>
						</div>
					</a>
				</div>
				<div class="tab-content form-fale" id="v-pills-tabContent">
					<div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
						<?php the_field( 'formulario_fale_conosco' ); ?>
						<script>
							// jQuery(window).load(function(){
							// 	console.log("runrun")		
							// 	jQuery("input[name=arquivo]").change(function() {
							// 	console.log("changed")
							// 		jQuery(this).parent().next(".btn-file").html('<span style="color: #13bafe;font-weight: bold;font-size: 11px;text-align: center !important;width: 100%;display: block;">--- Arquivo anexado ---<i class="fa fa-upload"></i></span>')
							// 		});
							// })
							$(window).on('load', function(){
								jQuery("input[name=arquivo]").change(function() {
								console.log("changed")
									jQuery(this).parent().next(".btn-file").html('<span style="color: #13bafe;font-weight: bold;font-size: 11px;text-align: center !important;width: 100%;display: block;margin-left: -15px;">Arquivo anexado<i class="fa fa-upload"></i></span>')
									});
							})
						</script>
					</div>
					<div class="tab-pane fade form-tra" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
						<?php the_field( 'formulario_trabalhe_conosco' ); ?>		
					</div>
				</div>
			<div class="contacts col-md-6 order-2">
				<p><?php $icon_telefone = get_field( 'icon_telefone', 'option' ); ?>
                        <?php if ( $icon_telefone ) : ?>
                            <img src="<?php echo esc_url( $icon_telefone['url'] ); ?>" alt="<?php echo esc_attr( $icon_telefone['alt'] ); ?>" loading='lazy'/>
                        <?php endif; ?></i><?php the_field( 'telefone', $page_ID ); ?></p>
				<?php if ( have_rows( 'cadastro_de_e-mails', $page_ID ) ) : ?>
					<?php while ( have_rows( 'cadastro_de_e-mails' ) ) : the_row(); ?>
					<p>
					<?php $icon_e_mail = get_field( 'icon_e-mail', 'option' ); ?>
                        <?php if ( $icon_e_mail ) : ?>
                            <img src="<?php echo esc_url( $icon_e_mail['url'] ); ?>" alt="<?php echo esc_attr( $icon_e_mail['alt'] ); ?>" loading='lazy' />
                        <?php endif; ?>  
					<?php the_sub_field( 'e-mail' ); ?> (<?php the_sub_field( 'titulo' ); ?>)</p>
					<?php endwhile; ?>
				<?php else : ?>
					<?php // no rows found ?>
				<?php endif; ?>
			</div>
			<div class="cp-obg col-md-6 order-md-2">
				<p>*Campos obrigatórios.</p>
			</div>
		</div>
	</div>
</section>



<?php get_footer(); ?>