
       var header = $("#nav_main");
        $(window).scroll(function () {
            var scroll = $(window).scrollTop();
            if (scroll >= 50) {
                header.addClass("scrolled");
            } else {
                header.removeClass("scrolled");
            }
        });
        $('#bs-example-navbar-collapse-1').on('show.bs.collapse', function () {
            $(".navbar-collapse").addClass("show");
        })
          
        $('#bs-example-navbar-collapse-1').on('hide.bs.collapse', function () {
            $(".navbar-collapse").removeClass("show");
        })
       
        /*==============================================================
        navbar fixed top
        ==============================================================*/
        // Hide Header on on scroll down
        var didScroll;
        var lastScrollTop = 0;
        var delta = 100;
        var navbarHeight = $('#nav_main').outerHeight();

        $(window).scroll(function (event) {
            didScroll = true;
        });

        setInterval(function () {
            if (didScroll) {
                hasScrolled();
                didScroll = false;
            }
        }, 250);

        function hasScrolled() {
            var st = $(this).scrollTop();

            // Make sure they scroll more than delta
            if (Math.abs(lastScrollTop - st) <= delta)
                return;

            // If they scrolled down and are past the navbar, add class .nav-up.
            // This is necessary so you never see what is "behind" the navbar.
            if (st > lastScrollTop && st > navbarHeight) {
                // Scroll Down
                $('.navbar').removeClass('nav-down').addClass('nav-up');
            } else {
                // Scroll Up
                if (st + $(window).height() < $(document).height()) {
                    $('.navbar').removeClass('nav-up').addClass('nav-down');
                }
            }

            lastScrollTop = st;
        }

// var behavior = function (val) {
//     return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
// },
//     options = {
//         onKeyPress: function (val, e, field, options) {
//             field.mask(behavior.apply({}, arguments), options);
//         }
//     };

// $('input.tel, input.wpcf7-tel').mask(behavior, options);
$('.carousel_empreendimentos').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 3,
    arrows: false,
    slidesToScroll: 1,
    dots: true,
    infinite: true,
    cssEase: 'linear',
    adaptiveHeight: true
});
$('.banner_slick').slick({
    dots: false,
    infinite: true,
    speed: 1500,
    slidesToShow: 1,
    arrows: false,
    slidesToScroll: 1,
    cssEase: 'linear',
    adaptiveHeight: true,
    autoplay: true,
    autoplaySpeed: 2000,
    fade: true
});

// $('.carousel_featured').slick({
//     dots: false,
//     slidesPerRow: 3,
//     rows: 4,
//     responsive: [
//         {
//             breakpoint: 767,
//             settings: {
//                 slidesPerRow: 1,
//                 rows: 1,
//             }
//         }
//     ]
// });

/*
accordion
*/
$('#accordion .collapse').on('shown.bs.collapse', function (e) {
    $(this).prev().addClass('active');
    var $card = $(this).closest('.card');
    var $open = $($(this).data('parent')).find('.collapse.show');
    var additionalOffset = 60;
    if($card.prevAll().filter($open.closest('.card')).length !== 0)
    {
          additionalOffset =  $open.height();
    }
    $('html,body').animate({
      scrollTop: $card.offset().top - additionalOffset
    }, 500);
});
$('#accordion .collapse').on('hidden.bs.collapse', function () {
    $(this).prev().removeClass('active');
});

/* wire up shown event*/
$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  console.log("tab shown...");
});

/* read hash from page load and change tab*/
var hash = document.location.hash;
var prefix = "tab_";  
if (hash) {
  $('.nav-tabs a[href="'+hash.replace(prefix,"")+'"]').tab('show');

  var $boxMaster = $('#nav-tab');
  // e.preventDefault();
  $('html, body').animate({
      scrollTop: $boxMaster.offset().top
  }, 700);
  $boxMaster.animate({
      scrollTop: $(this.hash).offset().top - $boxMaster.position().top + $boxMaster.scrollTop()
  }, 700);

}




$('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav'
});
$('.slider-for').slickLightbox({
    itemSelector        : 'a',
    navigateByKeyboard  : true
  });
$('.slider-nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    vertical:true,
    asNavFor: '.slider-for',
    dots: false,
    focusOnSelect: true,
    verticalSwiping:true,
    responsive: [
    {
        breakpoint: 992,
        settings: {
          vertical: false,
        }
    },
    {
      breakpoint: 768,
      settings: {
        vertical: false,
      }
    },
    {
      breakpoint: 580,
      settings: {
        vertical: false,
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 380,
      settings: {
        vertical: false,
        slidesToShow: 2,
      }
    }
    ]
});

$(function() {
    $(".video").click(function () {
      var theModal = $(this).data("target"),
      videoSRC = $(this).attr("data-video"),
      videoSRCauto = videoSRC + "?modestbranding=1&rel=0&controls=0&showinfo=0&html5=1&autoplay=1";
      $(theModal + ' iframe').attr('src', videoSRCauto);
      $(theModal + ' button.close').click(function () {
        $(theModal + ' iframe').attr('src', videoSRC);
      });
    });
  });
