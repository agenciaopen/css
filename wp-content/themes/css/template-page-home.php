<?php
/**
*
* Template Name: home
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<div id="carouselbanner" class="carousel slide carousel-fade" data-ride="carousel" data-interval="5000">
  <div class="carousel-inner">

  <?php if ( have_rows( 'cadastro_de_imagens' ) ) : ?>
    <?php
        $cont = 1;
    ?>
        <?php while ( have_rows( 'cadastro_de_imagens' ) ) : the_row(); ?>
	                  <?php $imagem_de_fundo = get_sub_field( 'imagem_de_fundo' ); ?>
<style>
	@media screen and (max-width: 767px){
		.carousel-item_<?php echo $cont;?>{
			background-image: url('<?php echo esc_url( $imagem_de_fundo['url'] ); ?>');
		}
		.img-m{
			display: none;
		}
	}
	  </style>
            <div class="carousel-item_<?php echo $cont;?> carousel-item <?php echo $cont == 1 ? 'active' : ''; ?>">
                <?php if ( $imagem_de_fundo ) : ?>
                    <img class="img-m" src="<?php echo esc_url( $imagem_de_fundo['url'] ); ?>" alt="<?php echo esc_attr( $imagem_de_fundo['alt'] ); ?>" loading='lazy' />
                    <div class="row align-items-end">
                        <div class="col-md-10 text-right legenda">
                            <p class="text-white"><?php the_sub_field('legenda', $page_ID);?></p>
                        </div>
                    </div>

                <?php endif; ?> 
            </div>  
            <?php $cont++; ?>
        <?php endwhile; ?>
    <?php else : ?>
        <?php // no rows found ?>
    <?php endif; ?>
  </div>
</div>

<section class="main_banner_text">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h1><?php the_field( 'titulo_principal_copiar' ); ?></h1>
                <?php $botao_obras = get_field( 'botao_obras' ); ?>
                <?php if ( $botao_obras ) : ?>
                    <a href="<?php echo esc_url( $botao_obras['url'] ); ?>" target="<?php echo esc_attr( $botao_obras['target'] ); ?>"><?php echo esc_html( $botao_obras['title'] ); ?></a>
                <?php endif; ?>
            </div>
        </div>
        
    </div>
</section>

<section class="about">
    <div class="container-fluid">
        <div class="row d-flex justify-content-center mb-5 hiden-mobile">
            <div class="col-md-10">
                <img src='<?php the_field('logo_site', 'option') ?>' class='img-fluid' alt='<?php bloginfo( 'name' ); ?>' title='<?php bloginfo( 'name' ); ?>' loading='lazy'>
            </div>
        </div>
        <div class="row d-flex justify-content-end">
            <div class="col-md-4 content">
                <div class="about_card">
                    <div class="box-card">
                        <h2><?php the_field( 'titulo_quem_somos' ); ?></h2>
                        <p><?php the_field( 'descricao_quem_somos' ); ?></p>
                        <?php $botao_quem_somos = get_field( 'botao_quem_somos' ); ?>
                        <?php if ( $botao_quem_somos ) : ?>
                            <a href="<?php echo esc_url( $botao_quem_somos['url'] ); ?>" target="<?php echo esc_attr( $botao_quem_somos['target'] ); ?>"><?php echo esc_html( $botao_quem_somos['title'] ); ?></a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="col-md-7 card-img pr-0 hiden-mobile">
                <?php $imagem_da_obra_para_quem_somos = get_field( 'imagem_da_obra_para_quem_somos' ); ?>
                <?php if ( $imagem_da_obra_para_quem_somos ) : ?>
                    <img src="<?php echo esc_url( $imagem_da_obra_para_quem_somos['url'] ); ?>" alt="<?php echo esc_attr( $imagem_da_obra_para_quem_somos['alt'] ); ?>" loading='lazy'/>
                <?php endif; ?>
                <p><?php the_field( 'descricao_da_imagem_da_obra_de_quem_somos' ); ?></p>
            </div>
        </div>
    </div>
</section>

<section class="movie" style="background-image: url('<?php the_field( "imagem_de_fundo_video_solucao" ); ?>)" loading='lazy'>
    <div class="container-fluid pl-0 pr-0">
        <div class="row">
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <?php $icon_close = get_field( 'icon_close', 'option' ); ?>
                        <?php if ( $icon_close ) : ?>
                            <img src="<?php echo esc_url( $icon_close['url'] ); ?>" alt="<?php echo esc_attr( $icon_close['alt'] ); ?>" loading='lazy'/>
                        <?php endif; ?>
                            </button>
                            <?php
                                $video = get_field( 'video_solucao' );
                            ?>
                            
                            <div class="youtube-video-place embed-responsive embed-responsive-4by3 " data-yt-url="<?php echo $video; ?>">
                                <iframe allowfullscreen frameborder="0" class="embed-responsive-item" src="<?php echo $video;?>"></iframe>
                                <?php $video_thumbnail = get_field( 'video_thumbnail' ); ?>
                                <?php if ( $video_thumbnail ) : ?>
                                    <img src="<?php echo esc_url( $video_thumbnail['url'] ); ?>" alt="<?php echo esc_attr( $video_thumbnail['alt'] ); ?>" async class="play-youtube-video d-none" loading='lazy'/>
                                <?php endif; ?>
                            </div>
                            <script>    
                            jQuery('.play-youtube-video')[0].click();
                            $(".play-youtube-video").trigger("click");

                            var video_wrapper = $('.youtube-video-place');
                            /*  Check to see if youtube wrapper exists*/
                            if(video_wrapper.length){
                            /*If user clicks on the video wrapper load the video.*/
                            $('.play-youtube-video').on('click', function(){
                            /* Dynamically inject the iframe on demand of the user.
                            Pull the youtube url from the data attribute on the wrapper element. */
                            video_wrapper.html('<iframe allowfullscreen frameborder="0" class="embed-responsive-item" src="' + video_wrapper.data('yt-url') + '"></iframe>');
                            });
                            }
                            </script>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5 movie-content">
            <button type="button" class="play_movie" data-toggle="modal" data-target="#exampleModal">
                <?php $icon_pay = get_field( 'icon_pay', 'option' ); ?>
                <?php if ( $icon_pay ) : ?>
                    <img src="<?php echo esc_url( $icon_pay['url'] ); ?>" alt="<?php echo esc_attr( $icon_pay['alt'] ); ?>" loading='lazy'/>
                <?php endif; ?>
            </button>
            <h3><?php the_field( 'titulo_video_solucao' ); ?></h3>
            </div>
        </div>
                    
    </div>
</section>

<section class="atuacao">
    <div class="container-fluid">
        <div class="row d-flex justify-content-center">
            <div class="col-md-2 card-content">
                <h2><?php the_field( 'titulo_areas_de_atuacao' ); ?></h2>
                <p><?php the_field( 'descricao_areas_de_atuacao' ); ?></p>
            </div>
            <div class="col-md-5 col-lg-6">
                <div class="six-column">
                    <ul class="nav nav-tabs-home">
                    <li>
                            <a href="/obras#nav-1">
                                <i class="hospitais" alt="Ícone de um hospital para simbolizar a atuação da CSS com soluções de engenharia para o segmento hospitalar. "></i>
                                <p>HOSPITAIS</p>
                                <span class="see_more">VER MAIS</span>
                            </a>
                        </li>
                        <li>
                            <a href="/obras#nav-2">
                                <i class="energia" alt="Ícone de uma torre de energia para simbolizar a atuação da CSS em obras de linhas de transmissão e energia. "></i>
                                <p>ENERGIA</p>  
                                <span class="see_more">VER MAIS</span>
                            </a>
                        </li>        
                        <li>
                            <a href="/obras#nav-3">
                                <i class="edificacoes" alt="Ícone com um guindaste e um prédio para simbolizar a atuação da CSS na construção de edificações. ">
                                </i>
                                <p>EDIFICAÇÕES</p>
                                <span class="see_more">VER MAIS</span>
                            </a>
                        </li>
                        <li>
                            <a href="/obras#nav-4">
                                <i class="industria" alt="Ícone de uma fábrica para simbolizar a atuação da CSS em soluções de engenharia para o segmento industrial. "></i>
                                <p>INDUSTRIAIS</p>
                                <span class="see_more">VER MAIS</span>
                            </a>
                        </li>
                        <li>
                            <a href="/obras#nav-5">
                                <i class="infraestrutura" alt="Ícone de um ônibus ao lado de uma parada para simbolizar a atuação da CSS em soluções de engenharia para infraestrutura. "></i>
                                <p style="margin-left: -20px;">INFRAESTRUTURA </p>
                                <span class="see_more">VER MAIS</span>
                            </a>
                        </li>
                        <li>
                            <a href="/obras#nav-6">
                                <i class="rodovias" alt="Ícone de uma estrada para simbolizar a atuação da CSS no segmento rodoviário. "></i>
                                <p>RODOVIAS</p> 
                                <span class="see_more">VER MAIS</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contact">
    <div class="container">
        <div class="row">
            <div class="col-md-4 card-content">
                <h2><?php the_field( 'titulo_fale_com_a_gente' ); ?></h2>
                <p><?php the_field( 'descricao_fale_com_a_gente' ); ?></p>
                <span><?php $icon_telefone = get_field( 'icon_telefone', 'option' ); ?>
                        <?php if ( $icon_telefone ) : ?>
                            <img src="<?php echo esc_url( $icon_telefone['url'] ); ?>" alt="<?php echo esc_attr( $icon_telefone['alt'] ); ?>" loading='lazy'/>
                        <?php endif; ?> <?php the_field( 'telefone_fale_com_a_gente' ); ?></span><br>
                <span>
                <?php $icon_e_mail = get_field( 'icon_e-mail', 'option' ); ?>
                        <?php if ( $icon_e_mail ) : ?>
                            <img src="<?php echo esc_url( $icon_e_mail['url'] ); ?>" alt="<?php echo esc_attr( $icon_e_mail['alt'] ); ?>" loading='lazy' />
                        <?php endif; ?>  
                <?php the_field( 'e-mail_fale_com_a_gente' ); ?></span>
            </div>
            <div class="col-md-7 pr-4 card-contact">
                <?php $imagem_fale_com_a_gente = get_field( 'imagem_fale_com_a_gente' ); ?>
                <?php if ( $imagem_fale_com_a_gente ) : ?>
                    <img class="hiden-mobile" src="<?php echo esc_url( $imagem_fale_com_a_gente['url'] ); ?>" alt="<?php echo esc_attr( $imagem_fale_com_a_gente['alt'] ); ?>" loading='lazy'/>
                <?php endif; ?>
                <?php the_field("formulario_de_contato_fale_com_a_gente") ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>