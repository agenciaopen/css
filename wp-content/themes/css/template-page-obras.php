<?php
/**
*
* Template Name: obras
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?> 

<div id="carouselbanner" class="carousel slide carousel-fade" data-ride="carousel" data-interval="5000">
  <div class="carousel-inner">

  <?php if ( have_rows( 'cadastro_de_imagens_de_fundo', $page_ID ) ) : ?>
    <?php
        $cont = 1;
    ?>
        <?php while ( have_rows( 'cadastro_de_imagens_de_fundo', $page_ID ) ) : the_row(); ?>
	  <style>
	@media screen and (max-width: 767px){
		.carousel-item_<?php echo $cont;?>{
			background-image: url('<?php the_sub_field( 'imagem' ); ?>');
		}
		.img-m{
			display: none;
		}
	}
	  </style>
            <div class="carousel-item_<?php echo $cont;?> carousel-item <?php echo $cont == 1 ? 'active' : ''; ?>">
                <?php if ( get_sub_field( 'imagem' ) ) : ?>
                    <img class="img-m" src="<?php the_sub_field( 'imagem' ); ?>" loading='lazy'/>
                <?php endif ?>
                <div class="row align-items-end">
                        <div class="col-md-10 text-right legenda">
                            <p class="text-white"><?php the_sub_field('legenda', $page_ID);?></p>
                        </div>
                    </div>
            </div>  
            <?php $cont++; ?>
        <?php endwhile; ?>
    <?php else : ?>
        <?php // no rows found ?>
    <?php endif; ?>
  </div>
</div>

<section class="main_text_obras">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?php the_field( 'titulo_obras', $page_ID ); ?></h1>
                <p><?php the_field( 'descricao_obras', $page_ID ); ?></p>
                <?php $botao_obras = get_field( 'botao_obras' ); ?>
                <?php if ( $botao_obras ) : ?>
                    <a href="<?php echo esc_url( $botao_obras['url'] ); ?>" target="<?php echo esc_attr( $botao_obras['target'] ); ?>"><?php echo esc_html( $botao_obras['title'] ); ?></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<section class="posts" id="product">
    <div class="container-fluid h-100">
        <div class="row h-100 align-items-start justify-content-center">
            <div class="col-md-8">
				<h2 class="color_main mb-4">
					Áreas de Atuação
				</h2>
            <p><?php the_field( 'descricao_listagem_de_obras',  $page_ID ); ?></p>
            </div>
            <div class="col-md-10 product-header pb-5">
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <?php
                        $terms = get_terms( 
                            array(
                            'taxonomy' => 'tipo',
                            'orderby' => 'slug',
                            
                            ) 
                        );
                        if( $terms):
                            $cont = 1;
                            foreach( $terms as $t ):
                    ?>
                    
                    <a class="nav-item nav-link <?php echo $cont == 1 ? 'active' : ''; ?>" id="nav-<?php echo $cont; ?>-tab" data-toggle="tab" href="#nav-<?php echo $cont; ?>" role="tab" aria-controls="nav-<?php echo $cont; ?>" aria-selected="<?php echo $cont == 1 ? 'true' : 'false'; ?>">
                        <?php
                            $taxonomy_prefix = 'tipo';
                            $term_id = $t->term_id ;
                            $term_id_prefixed = $taxonomy_prefix .'_'. $term_id;

                            ?>
                            <?php $icone = get_field( 'icone', $term_id_prefixed ); ?>
                            <?php if ( $icone ) : ?>
                                <img src="<?php echo esc_url( $icone['url'] ); ?>" alt="<?php echo esc_attr( $icone['alt'] ); ?>" loading='lazy'/>
                            <?php endif; ?> 
                            <span class="eael-tab-title" style="text-transform:uppercase"><?php echo $t->name; ?></span>

                    </a>
                    <?php
                                $cont++;
                            endforeach;
                        endif;
                    ?>
                </div>
            </div>
            <div class="col-md-9 tab-content" id="nav-tabContent">
                <?php
                    $terms = get_terms( 
                        array(
                        'taxonomy' => 'tipo',
                        'orderby' => 'slug',
                        
                        ) 
                    );
                    if( $terms):
                        $cont = 1;
                        foreach( $terms as $t ):
                ?>
                <div class="tab-pane fade <?php echo $cont == 1 ? 'show active' : ''; ?>" id="nav-<?php echo $cont; ?>" role="tabpanel" aria-labelledby="nav-<?php echo $cont; ?>-tab">
                    
                    <div class="row d-flex justify-content-between">
                        <?php
                        $post = get_posts( 
                            array(
                                'showposts' => -1,
                                'post_type' => 'obras',
                                'orderby' => 'date',
                                'order'   => 'ASC',
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'tipo',
                                        'field'    => 'slug',
                                        'terms'    => $t->slug,
                                        
                                    ),
                                )
                            ) 
                        ); ?>
                        <div class="col-md-6">
                            <h2 class="nat-text-title"><?php echo $t->name; ?></h2><br>
                            <p class="nav-text"><?php echo $t->description; ?></p>
                        </div>
                        <div class="col-md-6">
                        
                            <?php  
                                if ( $post ) {
                                    foreach ( $post as $post ) :
                                        setup_postdata( $post ); 
                                        $featured_img_url = get_the_post_thumbnail_url(get_the_ID($post->ID),'medium'); 
                                        $title_rede = get_the_title($post->ID);
                                        $video = get_field('ativar_video', $postid);
                                        //var_dump($post);
                            ?>
                                <div class="col-md-12">
                                    <div class="galery-slick galery-slick<?php echo $post->ID;?>">
 
                                        <?php $cadastro_de_fotos_images2 = get_field( 'cadastro_de_fotos2' ); ?>
                                        <?php if ( $cadastro_de_fotos_images2 ) :  ?>
                                            <?php foreach ( $cadastro_de_fotos_images2 as $cadastro_de_fotos_image2 ): ?>
                                                <a href="<?php echo esc_url( $cadastro_de_fotos_image2['url'] ); ?>">
                                                    <img src="<?php echo esc_url( $cadastro_de_fotos_image2['url'] ); ?>" alt="<?php echo esc_attr( $cadastro_de_fotos_image2['alt'] ); ?>" class="img-fluid" loading='lazy'/>
                                                </a>
                                            <?php endforeach; ?>
                                        <?php endif; ?>

                                        <?php if ($video == true) {
                                            $url =  get_field( 'youtube' );
                                            preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
                                            $youtube_id = $match[1];
                                            ?>
                                            <a href="https://www.youtube.com/embed/<?php echo $youtube_id;?>" target="_blank" class="thumbnail">
                                                <img src="<?php echo $featured_img_url;?>" class="img-fluid" loading='lazy'>
                                            </a>
                                            <?php } else {} ?>
                                        </div>
                                    <p class="nav-img-description">
                                        <?php echo $title_rede;?><br>
                                        <?php the_field( 'local' ); ?><br>
                                        <?php the_field( 'cidadeestado' ); ?>
                                    </p>
                                       
                                </div>
                            <?php

                                endforeach; 
                                wp_reset_postdata();
                            }?>
                        </div>
                    </div>

                </div>
                <?php
                            $cont++;
                        endforeach;
                    endif;
                ?>
            </div> 
        </div>

              
    </div>
</section><!-- /.content article -->

<section class="project">
    <div class="container-fluid">
        <div class="row d-flex justify-content-between">
            <div class="col-md-4 card-content">
                <h2><?php the_field( 'titulo_podemos_executar_o_seu_projeto',  $page_ID  ); ?></h2>
                <p><?php the_field( 'descricao_podemos_executar_o_seu_projeto',  $page_ID  ); ?></p>
                <div class="contact-medium">
                    <p><?php $icon_telefone = get_field( 'icon_telefone', 'option' ); ?>
                        <?php if ( $icon_telefone ) : ?>
                            <img src="<?php echo esc_url( $icon_telefone['url'] ); ?>" alt="<?php echo esc_attr( $icon_telefone['alt'] ); ?>" loading='lazy'/>
                        <?php endif; ?><?php the_field( 'telefone_podemos_executar_seu_projeto',  $page_ID  ); ?></p>
                    <p><?php $icon_e_mail = get_field( 'icon_e-mail', 'option' ); ?>
                        <?php if ( $icon_e_mail ) : ?>
                            <img src="<?php echo esc_url( $icon_e_mail['url'] ); ?>" alt="<?php echo esc_attr( $icon_e_mail['alt'] ); ?>" loading='lazy' />
                        <?php endif; ?>  <?php the_field( 'e-mail_podemos_executar_seu_projeto',  $page_ID  ); ?></p>
                </div>
            </div>
            <div class="col-md-6 pl-0 pr-4 card-contact">
                <div class="col-md-3 p-0 img-contact">
                    <?php if ( get_field( 'imagem_podemos_executar_seu_projeto' ) ) : ?>
                        <img src="<?php the_field( 'imagem_podemos_executar_seu_projeto',  $page_ID  ); ?>" loading='lazy'/>
                    <?php endif ?>
                </div>
                <div class="col-md-8 form-contact">
                <?php the_field( 'formulario_podemos_executar_seu_projeto',  $page_ID  ); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
<?php
                    $terms = get_terms( 
                        array(
                        'taxonomy' => 'tipo',
                        'orderby' => 'slug',
                        
                        ) 
                    );
                    if( $terms):
                        $cont = 1;
                        foreach( $terms as $t ):
                ?>
                    
                        <?php
                        $post = get_posts( 
                            array(
                                'showposts' => -1,
                                'post_type' => 'obras',
                                'orderby' => 'date',
                                'order'   => 'ASC',
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'tipo',
                                        'field'    => 'slug',
                                        'terms'    => $t->slug,
                                        
                                    ),
                                )
                            ) 
                        ); ?>
                      
                        
                            <?php  
                                if ( $post ) {
                                    foreach ( $post as $post ) :
                                        setup_postdata( $post ); 
                                       
                            ?>
                              
                              <script>
                                        $('.galery-slick<?php echo $post->ID;?>').slick({
                                            dots: false,
                                            infinite: false,
                                            speed: 900,
                                            slidesToShow: 1,
                                            arrows: false,
                                            slidesToScroll: 1,
                                            //adaptiveHeight: true,
                                            autoplay:true,
                                            autoplaySpeed: 1500,
                                            speed:3000,
											responsive: [
    {
    
      breakpoint: 767,
      settings: {
vertical:true,
		verticalSwiping: true
      }
    }  ]

                                        });
                                        $('.galery-slick<?php echo $post->ID;?>').slickLightbox({
                                            itemSelector        : 'a',
                                            navigateByKeyboard  : true
                                        });
                                    </script>
<?php
                                endforeach; 
                                wp_reset_postdata();
                            }?>
               
                <?php
                            $cont++;
                        endforeach;
                    endif;
                ?>

