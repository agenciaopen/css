<?php
/**
 * Plugin Name: css construtora
 * Plugin URI: https://cssconstrutora.com
 * Description: post types plugin
 * Version: 1.0
 * Author: agencia open
 * Author URI: https://www.agenciaopen.com
 */

 /*
* Creating a function to create our CPT
*/
add_theme_support('post-thumbnails');

function obras_custom_post_type() {
 
    // Set UI labels for Custom Post Type
        $labels = array(
            'name'                => _x( 'Obras', 'Post Type General Name', 'open' ),
            'singular_name'       => _x( 'Obra', 'Post Type Singular Name', 'open' ),
            'menu_name'           => __( 'Obras', 'open' ),
            'parent_item_colon'   => __( 'Parent Obra', 'open' ),
            'all_items'           => __( 'Todos os Obras', 'open' ),
            'view_item'           => __( 'Ver Obra', 'open' ),
            'add_new_item'        => __( 'Adicionar novo Obra', 'open' ),
            'add_new'             => __( 'Adicionar novo', 'open' ),
            'edit_item'           => __( 'Editar Obra', 'open' ),
            'update_item'         => __( 'Atualizar Obra', 'open' ),
            'search_items'        => __( 'Procurar Obra', 'open' ),
            'not_found'           => __( 'Nada encontrado', 'open' ),
            'not_found_in_trash'  => __( 'Nada encontrada na lixeira', 'open' ),
        );
         
    // Set other options for Custom Post Type
         
        $args = array(
            'label'               => __( 'Obras', 'open' ),
            'description'         => __( 'Cadastro de Obras e seus detalhes', 'open' ),
            'labels'              => $labels,
            // Features this CPT supports in Post Editor
            'supports'            => array( 'title', 'author', 'thumbnail', 'revisions', 'custom-fields'),
            /* A hierarchical CPT is like Pages and can have
            * Parent and child items. A non-hierarchical CPT
            * is like Posts.
            */ 
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'can_export'          => true,
            'has_archive'         => false,
            'exclude_from_search' => true,
            'menu_icon'           => 'dashicons-welcome-add-page',
            'publicly_queryable'  => true,
            'capability_type'     => 'post',
            'show_in_rest' => true,
      
        );
         
        // Registering your Custom Post Type
        register_post_type( 'obras', $args );
     
    }
     
    /* Hook into the 'init' action so that the function
    * Containing our post type registration is not 
    * unnecessarily executed. 
    */
     
    add_action( 'init', 'obras_custom_post_type', 0 );

    add_action( 'init', 'open_create_obras_custom_taxonomy_status', 0 );

    //create a custom taxonomy 
function open_create_obras_custom_taxonomy_status() {
 
    $labels = array(
        'name' => _x( 'Tipos de obra', 'taxonomy general name' ),
        'singular_name' => _x( 'Tipos de obra', 'taxonomy singular name' ),
        'search_items' =>  __( 'Procurar Tipos de obra' ),
        'all_items' => __( 'Todas os Tipos de obra' ),
        'parent_item' => __( 'Parent Tipos de obra' ),
        'parent_item_colon' => __( 'Parent Tipos de obra:' ),
        'edit_item' => __( 'Editar Tipos de obra' ), 
        'update_item' => __( 'Atualizar Tipos de obra' ),
        'add_new_item' => __( 'Adicionar novo Tipos de obra' ),
        'new_item_name' => __( 'Novo nome para Tipos de obra' ),
        'menu_name' => __( 'Tipos de obra' ),
    ); 	
 
    register_taxonomy('tipo',array('obras'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'tipo-de-obra' ),
    ));
}