WebP Express 0.19.0. Conversion triggered using bulk conversion, 2021-01-13 22:20:12

*WebP Convert 2.3.2*  ignited.
- PHP version: 7.3.21
- Server software: Apache/2.4.46 (Win64) PHP/7.3.21

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: C:\workspace\open\css/wp-content/uploads/2021/01/fundo-hospital.png
- destination: C:\workspace\open\css/wp-content/webp-express/webp-images/uploads/2021\01\fundo-hospital.png.webp
- log-call-arguments: true
- converters: (array of 10 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- alpha-quality: 80
- encoding: "auto"
- metadata: "none"
- near-lossless: 60
- quality: 85
------------


*Trying: cwebp* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: C:\workspace\open\css/wp-content/uploads/2021/01/fundo-hospital.png
- destination: C:\workspace\open\css/wp-content/webp-express/webp-images/uploads/2021\01\fundo-hospital.png.webp
- alpha-quality: 80
- encoding: "auto"
- low-memory: true
- log-call-arguments: true
- metadata: "none"
- method: 6
- near-lossless: 60
- quality: 85
- use-nice: true
- command-line-options: ""
- try-common-system-paths: true
- try-supplied-binary-for-os: true

The following options have not been explicitly set, so using the following defaults:
- auto-filter: false
- default-quality: 85
- max-quality: 85
- preset: "none"
- size-in-percentage: null (not set)
- skip: false
- rel-path-to-precompiled-binaries: *****
- try-cwebp: true
- try-discovering-cwebp: true
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' n�o � reconhecido como um comando interno
ou externo, um programa oper�vel ou um arquivo em lotes.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- C:\workspace\open\css\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: C:\workspace\open\css\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -version 2>&1. Result: version: *1.1.0*
Binaries ordered by version number.
- C:\workspace\open\css\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe: (version: 1.1.0)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.1.0
Quality: 85. 
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
C:\workspace\open\css\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -metadata none -q 85 -alpha_q "80" -m 6 -low_memory "C:\workspace\open\css/wp-content/uploads/2021/01/fundo-hospital.png" -o "C:\workspace\open\css/wp-content/webp-express/webp-images/uploads/2021\01\fundo-hospital.png.webp.lossy.webp" 2>&1 2>&1

*Output:* 
Saving file 'C:\workspace\open\css/wp-content/webp-express/webp-images/uploads/2021\01\fundo-hospital.png.webp.lossy.webp'
File:      C:\workspace\open\css/wp-content/uploads/2021/01/fundo-hospital.png
Dimension: 1920 x 795
Output:    83974 bytes Y-U-V-All-PSNR 43.31 47.20 47.51   44.29 dB
           (0.44 bpp)
block count:  intra4:       2238  (37.30%)
              intra16:      3762  (62.70%)
              skipped:      2558  (42.63%)
bytes used:  header:            360  (0.4%)
             mode-partition:  12372  (14.7%)
 Residuals bytes  |segment 1|segment 2|segment 3|segment 4|  total
  intra4-coeffs:  |   46106 |    1278 |    1322 |     352 |   49058  (58.4%)
 intra16-coeffs:  |    3429 |    1681 |    2759 |     488 |    8357  (10.0%)
  chroma coeffs:  |   11115 |     810 |    1445 |     431 |   13801  (16.4%)
    macroblocks:  |      34%|       7%|      15%|      43%|    6000
      quantizer:  |      20 |      16 |      12 |       8 |
   filter level:  |      13 |      15 |      15 |       9 |
------------------+---------+---------+---------+---------+-----------------
 segments total:  |   60650 |    3769 |    5526 |    1271 |   71216  (84.8%)

Success
Reduction: 69% (went from 261 kb to 82 kb)

Converting to lossless
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' n�o � reconhecido como um comando interno
ou externo, um programa oper�vel ou um arquivo em lotes.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- C:\workspace\open\css\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: C:\workspace\open\css\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -version 2>&1. Result: version: *1.1.0*
Binaries ordered by version number.
- C:\workspace\open\css\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe: (version: 1.1.0)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.1.0
Trying to convert by executing the following command:
C:\workspace\open\css\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -metadata none -q 85 -alpha_q "80" -near_lossless 60 -m 6 -low_memory "C:\workspace\open\css/wp-content/uploads/2021/01/fundo-hospital.png" -o "C:\workspace\open\css/wp-content/webp-express/webp-images/uploads/2021\01\fundo-hospital.png.webp.lossless.webp" 2>&1 2>&1

*Output:* 
Saving file 'C:\workspace\open\css/wp-content/webp-express/webp-images/uploads/2021\01\fundo-hospital.png.webp.lossless.webp'
File:      C:\workspace\open\css/wp-content/uploads/2021/01/fundo-hospital.png
Dimension: 1920 x 795
Output:    259436 bytes (1.36 bpp)
Lossless-ARGB compressed size: 259436 bytes
  * Header size: 1521 bytes, image data size: 257889
  * Lossless features used: PALETTE
  * Precision Bits: histogram=5 transform=4 cache=3
  * Palette size:   256

Success
Reduction: 3% (went from 261 kb to 253 kb)

Picking lossy
cwebp succeeded :)

Converted image in 1367 ms, reducing file size with 69% (went from 261 kb to 82 kb)
