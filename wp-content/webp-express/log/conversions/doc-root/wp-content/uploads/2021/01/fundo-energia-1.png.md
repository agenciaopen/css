WebP Express 0.19.0. Conversion triggered using bulk conversion, 2021-01-13 22:20:02

*WebP Convert 2.3.2*  ignited.
- PHP version: 7.3.21
- Server software: Apache/2.4.46 (Win64) PHP/7.3.21

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: C:\workspace\open\css/wp-content/uploads/2021/01/fundo-energia-1.png
- destination: C:\workspace\open\css/wp-content/webp-express/webp-images/uploads/2021\01\fundo-energia-1.png.webp
- log-call-arguments: true
- converters: (array of 10 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- alpha-quality: 80
- encoding: "auto"
- metadata: "none"
- near-lossless: 60
- quality: 85
------------


*Trying: cwebp* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: C:\workspace\open\css/wp-content/uploads/2021/01/fundo-energia-1.png
- destination: C:\workspace\open\css/wp-content/webp-express/webp-images/uploads/2021\01\fundo-energia-1.png.webp
- alpha-quality: 80
- encoding: "auto"
- low-memory: true
- log-call-arguments: true
- metadata: "none"
- method: 6
- near-lossless: 60
- quality: 85
- use-nice: true
- command-line-options: ""
- try-common-system-paths: true
- try-supplied-binary-for-os: true

The following options have not been explicitly set, so using the following defaults:
- auto-filter: false
- default-quality: 85
- max-quality: 85
- preset: "none"
- size-in-percentage: null (not set)
- skip: false
- rel-path-to-precompiled-binaries: *****
- try-cwebp: true
- try-discovering-cwebp: true
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' n�o � reconhecido como um comando interno
ou externo, um programa oper�vel ou um arquivo em lotes.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- C:\workspace\open\css\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: C:\workspace\open\css\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -version 2>&1. Result: version: *1.1.0*
Binaries ordered by version number.
- C:\workspace\open\css\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe: (version: 1.1.0)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.1.0
Quality: 85. 
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
C:\workspace\open\css\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -metadata none -q 85 -alpha_q "80" -m 6 -low_memory "C:\workspace\open\css/wp-content/uploads/2021/01/fundo-energia-1.png" -o "C:\workspace\open\css/wp-content/webp-express/webp-images/uploads/2021\01\fundo-energia-1.png.webp.lossy.webp" 2>&1 2>&1

*Output:* 
Saving file 'C:\workspace\open\css/wp-content/webp-express/webp-images/uploads/2021\01\fundo-energia-1.png.webp.lossy.webp'
File:      C:\workspace\open\css/wp-content/uploads/2021/01/fundo-energia-1.png
Dimension: 1920 x 795
Output:    71120 bytes Y-U-V-All-PSNR 44.27 48.77 47.53   45.22 dB
           (0.37 bpp)
block count:  intra4:       2438  (40.63%)
              intra16:      3562  (59.37%)
              skipped:      2725  (45.42%)
bytes used:  header:            325  (0.5%)
             mode-partition:  13331  (18.7%)
 Residuals bytes  |segment 1|segment 2|segment 3|segment 4|  total
  intra4-coeffs:  |   32653 |    3500 |    3336 |     323 |   39812  (56.0%)
 intra16-coeffs:  |    1123 |    1267 |    2498 |     420 |    5308  (7.5%)
  chroma coeffs:  |    5999 |    2500 |    3185 |     634 |   12318  (17.3%)
    macroblocks:  |      21%|      13%|      20%|      46%|    6000
      quantizer:  |      20 |      17 |      14 |       9 |
   filter level:  |      18 |      20 |      23 |       7 |
------------------+---------+---------+---------+---------+-----------------
 segments total:  |   39775 |    7267 |    9019 |    1377 |   57438  (80.8%)

Success
Reduction: 73% (went from 255 kb to 69 kb)

Converting to lossless
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' n�o � reconhecido como um comando interno
ou externo, um programa oper�vel ou um arquivo em lotes.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- C:\workspace\open\css\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: C:\workspace\open\css\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -version 2>&1. Result: version: *1.1.0*
Binaries ordered by version number.
- C:\workspace\open\css\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe: (version: 1.1.0)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.1.0
Trying to convert by executing the following command:
C:\workspace\open\css\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -metadata none -q 85 -alpha_q "80" -near_lossless 60 -m 6 -low_memory "C:\workspace\open\css/wp-content/uploads/2021/01/fundo-energia-1.png" -o "C:\workspace\open\css/wp-content/webp-express/webp-images/uploads/2021\01\fundo-energia-1.png.webp.lossless.webp" 2>&1 2>&1

*Output:* 
Saving file 'C:\workspace\open\css/wp-content/webp-express/webp-images/uploads/2021\01\fundo-energia-1.png.webp.lossless.webp'
File:      C:\workspace\open\css/wp-content/uploads/2021/01/fundo-energia-1.png
Dimension: 1920 x 795
Output:    244250 bytes (1.28 bpp)
Lossless-ARGB compressed size: 244250 bytes
  * Header size: 1720 bytes, image data size: 242505
  * Lossless features used: PALETTE
  * Precision Bits: histogram=5 transform=4 cache=3
  * Palette size:   256

Success
Reduction: 6% (went from 255 kb to 239 kb)

Picking lossy
cwebp succeeded :)

Converted image in 1442 ms, reducing file size with 73% (went from 255 kb to 69 kb)
